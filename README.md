# Description

Here is the Meteor boilerplate for React development.
Features:
- easy load all the dependencies (see `package.json`)
- simplified routes definition (FlowRouter based)
- simplified dynamic page head properties (title, meta, scripts and stylesheets) definition
- automatic hot-reload of React components
- PostCSS enabled, Stylus-like syntax is used by default (w/o installing Stylus itself)
- Google Analytics enabled
- AgileCRM enabled as the scripts dynamic load example

Feel free to request more details in the README. Pull requests are welcome!


# How to get
Run from your projects directory:
```
git clone https://gitlab.com/priezz/meteor-react-hot.git
cd meteor-react-hot
```

# How to use

Just run from within `meteor-react-hot` directory:
```
npm start
```
It will install all the dependencies and run Meteor.
