import { Component } from "react"

import Layout from "/client/imports/layouts/Layout"

const Content = () =>
	<div className="content-wrapper">
		<header>
			<h1 className="title">Title</h1>
			<h4 className="subtitle">Subtitle</h4>
		</header>
		<div className="content">
			Some text here
		</div>
	</div>

export default class Home extends Component {
	render() {
		return <Layout content={ Content } />
	}
}
