import { Component, PropTypes } from "react"

import "/client/index.css"

export default class Layout extends Component {
    static propTypes = { content: PropTypes.any.isRequired }

    render() {
        const Content = this.props.content
        return <div className="page-wrapper"><Content/></div>
    }
}
