import { setHead, setRoutes } from "priezz_helpers"

/* Import is necessary to let the file be available for require from modules */
import "/client/imports/pages/Home"

const settings = Meteor.settings.public
const scripts = []

if( settings.agileCrm ) scripts.push( {
    /* Agile CRM */
    src: `https://${ settings.agileCrm.account }.agilecrm.com/stats/min/agile-min.js`,
    callback: () => {
        /* global _agile, _agile_execute_web_rules */
        if( !_agile ) return
        _agile.set_account( settings.agileCrm.token, settings.agileCrm.account )
        _agile.track_page_view()
        _agile_execute_web_rules()
    }
} )

setHead( {
    title: "Page Title.",
    meta: [
        { "http-equiv": "content-type", content: "text/html; charset=utf-8'" },
        { charset: "utf-8" },
    ],
    links: [
        { rel: "stylesheet", type: "text/css", href: "https://fonts.googleapis.com/css?family=Nunito:400,700,300" },
        { rel: "stylesheet", type: "text/css", href: "https://fonts.googleapis.com/css?family=Ubuntu:400,300,500,700" },
        { rel: "stylesheet", type: "text/css", href: "https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" },
    ],
    scripts
} )

/* Source, not component should be provided to enable hot-reload   */
/* provide `module.hot` as the last parameter to enable hot-reload */
setRoutes( {
    "/": { source: "/client/imports/pages/Home.jsx" },
}, module.hot )
